# Contributor: Celeste <cielesti@protonmail.com>
# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=mitra
pkgver=1.36.0
pkgrel=0
pkgdesc="ActivityPub microblogging platform written in Rust"
url="https://mitra.social/@mitra"
# riscv64: vite webapp fails to build: 'Parse error @:1:38'
arch="all !riscv64"
license="AGPL-3.0-only"
depends="postgresql"
makedepends="
	cargo
	cargo-auditable
	nodejs
	npm
	openssl-dev
	"
install="$pkgname.pre-install $pkgname.post-install"
pkgusers="mitra"
pkggroups="mitra"
subpackages="$pkgname-doc $pkgname-openrc"
source="mitra-$pkgver.tar.gz::https://codeberg.org/silverpill/mitra/archive/v$pkgver.tar.gz
	mitra-web-$pkgver.tar.gz::https://codeberg.org/silverpill/mitra-web/archive/v$pkgver.tar.gz
	mitra.initd
	init.sql
	config.yaml
	"
builddir="$srcdir/mitra"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked

	cd "$srcdir/mitra-web"

	npm ci --foreground-scripts
}

build() {
	cargo auditable build --frozen --release \
		--features production

	cd "$srcdir/mitra-web"

	echo 'VITE_BACKEND_URL=' > .env.local
	npm run build
}

check() {
	# These tests require a database connection
	cargo test --frozen --workspace \
		--exclude mitra-models -- \
		--skip test_follow_unfollow \
		--skip test_hide_reblogs \
		--skip test_subscribe_unsubscribe \
		--skip test_get_jrd

	cd "$srcdir/mitra-web"

	npm run test
}

package() {
	install -Dm755 target/release/mitra -t "$pkgdir"/usr/bin
	install -Dm755 target/release/mitractl -t "$pkgdir"/usr/bin

	mkdir -p "$pkgdir"/usr/share/webapps
	cp -r "$srcdir"/mitra-web/dist \
		"$pkgdir"/usr/share/webapps/mitra

	install -Dm644 docs/* -t "$pkgdir"/usr/share/doc/$pkgname
	install -Dm644 config.yaml.example \
		contrib/Caddyfile contrib/*.nginx \
		contrib/monero/wallet.conf \
		-t "$pkgdir"/usr/share/doc/$pkgname/examples

	install -Dm640 -g mitra "$srcdir"/config.yaml -t "$pkgdir"/etc/mitra
	install -dm755 -o mitra -g mitra "$pkgdir"/var/lib/mitra
	install -Dm644 "$srcdir"/init.sql -t "$pkgdir"/var/lib/mitra
	install -Dm755 "$srcdir"/mitra.initd "$pkgdir"/etc/init.d/mitra
}

sha512sums="
f1b1e7f57ae23be9ab163b179e3b302e9587b4fa27460f7a6fc34fd01c78805eae585c0df05bac6a1a37aa31e3483a7f6e23b68188e8a5e3d640edc3c3128281  mitra-1.36.0.tar.gz
6f82e74ad1a2e76c53f57c391220466432f97f0543ccf766649235aff2d2bec403bcf45987ae7c9b23ce9fe968671e00bd5eaa2681e3281967c55dfaf1d9f0ae  mitra-web-1.36.0.tar.gz
691f84f5dfdddc176e75792ab03ff167054246e75ced51be47a89f405ae55ebe5eb6280b73c1b467b5ecbe8539f6108fb3d86873d50fcc4f4b8c5b182632acb0  mitra.initd
02a40bf9dee625bf4b4783aa3588428313efa1e4d5ec5b8e64d76b9c6248f4d72b9d1287fba72b9d5d62c34352131d5d1609453583e257c5e8a1d2a787779147  init.sql
ad007b34c960d15e41e32644ca9d8e7ef4e9cb11b5db2ed43d04c5e913e732ccbdf0937ded8093a192e61a8c5a5b8aea5071018c62128076dd5341def2a95119  config.yaml
"

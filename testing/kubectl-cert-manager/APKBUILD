# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=kubectl-cert-manager
pkgver=1.13.0
pkgrel=0
pkgdesc="Manage and configure cert-manager resources for Kubernetes"
url="https://cert-manager.io/"
license="Apache-2.0"
arch="all !armhf !riscv64" # limited by kubectl
depends="kubectl"
makedepends="go"
source="https://github.com/cert-manager/cert-manager/archive/v$pkgver/kubectl-cert-manager-$pkgver.tar.gz"
builddir="$srcdir/cert-manager-$pkgver/cmd/ctl"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build \
		-ldflags "-X github.com/cert-manager/cert-manager/pkg/util.AppVersion=v$pkgver"
}

check() {
	go test ./...
}

package() {
	install -Dm755 ctl "$pkgdir"/usr/bin/kubectl-cert_manager
}

sha512sums="
10eaf9ccc058ea58ce718f166778f6a1570219273cbae0de4d847264413d36fd7d335f7c2fd9186f3328bfc77ebe4f789a548eb908cd4f15bb032bc6a7afda7d  kubectl-cert-manager-1.13.0.tar.gz
"

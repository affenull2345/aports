# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=flux
pkgver=2.1.0
pkgrel=1
pkgdesc="Open and extensible continuous delivery solution for Kubernetes"
url="https://fluxcd.io/"
arch="all"
license="Apache-2.0"
makedepends="go bash kustomize"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/fluxcd/flux2/archive/v$pkgver/flux-$pkgver.tar.gz"
builddir="$srcdir/flux2-$pkgver"

export CGO_ENABLED=0
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

# secfixes:
#   0.36.0-r0:
#     - CVE-2022-39272

build() {
	bash ./manifests/scripts/bundle.sh

	go build -v \
		-ldflags "-X main.VERSION=$pkgver" \
		./cmd/flux

	for shell in bash fish zsh; do
		./$pkgname completion $shell > $pkgname.$shell
	done
}

check() {
	# e2e tests require a Kubernetes cluster
	# /cmd/flux tests try to spawn a local Kubernetes cluster
	go test -tags=unit $(go list ./... | grep -v /cmd/flux)
}

package() {
	install -Dm755 $pkgname -t "$pkgdir"/usr/bin/

	install -Dm644 $pkgname.bash \
		"$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm644 $pkgname.fish \
		"$pkgdir"/usr/share/fish/vendor_completions.d/$pkgname.fish
	install -Dm644 $pkgname.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
72151bc0c27ccccf9ff986fa12b0cf3acdcfa1251817bf5b3e29519cbf5c57388484f2e50a39b58ce2c2bd82b2b74609963831722522b80cd0e77ba03067139b  flux-2.1.0.tar.gz
"

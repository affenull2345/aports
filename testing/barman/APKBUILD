# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=barman
pkgver=3.8.0
pkgrel=0
pkgdesc="Backup and recovery manager for PostgreSQL"
url="http://www.pgbarman.org"
arch="noarch"
license="GPL-3.0-or-later"
depends="python3 rsync postgresql-client py3-argcomplete py3-dateutil py3-psycopg2 py3-boto3"
makedepends="py3-setuptools"
subpackages="$pkgname-doc $pkgname-bash-completion $pkgname-pyc"
pkgusers="barman"
pkggroups="barman"
install="$pkgname.pre-install"
options="!check" #pytest does not start and I don't know why
checkdepends="py3-pytest-timeout py3-mock py3-pytest-runner py3-pip py3-mock"
source="$pkgname-$pkgver.tar.gz::https://github.com/EnterpriseDB/barman/releases/download/release/$pkgver/barman-$pkgver.tar.gz"

build() {
	python3 setup.py build
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"

	install -Dm0644 ./scripts/barman.bash_completion \
		"$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -d -o $pkgusers -g $pkggroups -m 755 "$pkgdir"/var/log/$pkgname

	cd doc
	install -Dm0644 barman.conf -t  "$pkgdir/etc"
}

sha512sums="
d189bfacc08b6e4d2828dad433a45358d19503fbe1714dcff431c10ffa147dfa0414a947dbb379ecaf5a3a6f401196f45a5f83ac508a128478f9a8ed45261f15  barman-3.8.0.tar.gz
"

# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=py3-msgpack
_pkgname=msgpack-python
pkgver=1.0.6
pkgrel=0
pkgdesc="Python3 module for MessagePack serialization/deserialization"
url="https://msgpack.org/"
arch="all"
license="Apache-2.0"
depends="python3"
makedepends="python3-dev py3-gpep517 py3-setuptools cython py3-wheel"
checkdepends="py3-pytest-xdist py3-pluggy"
subpackages="$pkgname-pyc"
source="$_pkgname-$pkgver.tar.gz::https://github.com/msgpack/msgpack-python/archive/v$pkgver.tar.gz"
builddir="$srcdir/msgpack-python-$pkgver"

replaces="py-msgpack" # Backwards compatibility
provides="py-msgpack=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest -n auto
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
e1d901546507f402b88ed30c7ea74e10d0e4030821ec727098c289906e65be9537dd316013a1eeb8e14fe684d5ed64dd70c95a11b5186f4340c65bf761811708  msgpack-python-1.0.6.tar.gz
"

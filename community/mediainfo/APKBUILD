# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=mediainfo
pkgver=23.09
pkgrel=0
pkgdesc="Supplies technical and tag information about media files"
url="https://mediaarea.net/en/MediaInfo"
arch="all"
license="BSD-2-Clause"
makedepends="
	autoconf
	automake
	libmediainfo-dev~=${pkgver%.*}
	libtool
	libzen-dev
	wxwidgets-dev
	"
subpackages="$pkgname-gui"
source="https://mediaarea.net/download/source/mediainfo/$pkgver/mediainfo_$pkgver.tar.xz"
builddir="$srcdir/MediaInfo"
_clidir="$builddir/Project/GNU/CLI"
_guidir="$builddir/Project/GNU/GUI"

prepare() {
	default_prepare

	rm -Rf "$builddir"/Project/MS*

	cd "$_clidir"
	sh ./autogen.sh
	cd "$_guidir"
	sh ./autogen.sh
}

build() {
	export CFLAGS="$CFLAGS -flto=auto"
	export CXXFLAGS="$CXXFLAGS -flto=auto"

	cd "$_clidir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--libdir=/usr/lib \
		--sysconfdir=/etc \
		--enable-static=no
	make
	cd "$_guidir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--libdir=/usr/lib \
		--sysconfdir=/etc
	make
}

check() {
	cd "$_clidir"
	./mediainfo --version
}

package() {
	pkgdesc="$pkgdesc (CLI)"
	make -C "$_clidir" DESTDIR="$pkgdir" install
	make -C "$_guidir" DESTDIR="$pkgdir" install

	cd "$pkgdir"
	# legacy
	rm -r usr/share/pixmaps
	rm -r usr/share/kde4
}

gui() {
	pkgdesc="$pkgdesc (GUI)"

	amove \
		usr/bin/mediainfo-gui \
		usr/share
}

sha512sums="
8b8211e542db6e7ed67c0b7a588ceb300918b365dc1fd89ff8ce7622e477206f0d52cfa1e429312fa483e7f322910e2e599a363fbfe48ecca407bbe144f9501b  mediainfo_23.09.tar.xz
"

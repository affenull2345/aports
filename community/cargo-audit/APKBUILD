# Contributor: kpcyrd <git@rxv.cc>
# Maintainer: kpcyrd <git@rxv.cc>
pkgname=cargo-audit
pkgver=0.18.1
pkgrel=0
pkgdesc="Audit Cargo.lock for crates with security vulnerabilities"
url="https://github.com/RustSec/rustsec"
# s390x, ppc64le, riscv64: blocked by ring crate
arch="all !s390x !ppc64le !riscv64"
license="MIT OR Apache-2.0"
makedepends="cargo libgit2-dev openssl-dev>3 cargo-auditable"
subpackages="$pkgname-doc"
options="net"
source="$pkgname-$pkgver.tar.gz::https://github.com/RustSec/cargo-audit/archive/cargo-audit/v$pkgver.tar.gz"
builddir="$srcdir/rustsec-$pkgname-v$pkgver/$pkgname"
options="net !check" # requires running binaries prebuilt against glibc as part of the tests and fails

prepare() {
	default_prepare

	# Rust target triple.
	local target=$(rustc -vV | sed -n 's/host: //p')

	# Build against system-provided libs
	mkdir -p .cargo
	cat >> .cargo/config.toml <<-EOF
		[target.$target]
		git2 = { rustc-link-lib = ["git2"] }
	EOF

	# no lock
	cargo fetch --target="$CTARGET"
}

build() {
	cargo auditable build --release --frozen --features fix
}

check() {
	cargo test --frozen
}

package() {
	install -Dm 755 ../target/release/cargo-audit -t "$pkgdir/usr/bin"
	install -Dm 644 -t "$pkgdir/usr/share/doc/cargo-audit" README.md
}

sha512sums="
c5513c6f95173a1637c69c0e1bc665bbecb6f5edc9ed3f3a7bcd7add800de631fdc01c8b7ec64adafa943ecfd2dddda97b7f175948a2463663537e6b86708ab2  cargo-audit-0.18.1.tar.gz
"

# Contributor: kohnish <kohnish@gmx.com>
# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=crun
pkgver=1.9
pkgrel=0
pkgdesc="Fast and lightweight fully featured OCI runtime and C library for running containers"
url="https://github.com/containers/crun"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
arch="all"
makedepends="libcap-dev libseccomp-dev yajl-dev argp-standalone python3 go-md2man"
subpackages="$pkgname-doc $pkgname-static"
source="https://github.com/containers/crun/releases/download/$pkgver/crun-$pkgver.tar.xz"

provides="oci-runtime"
provider_priority=100 # highest, default provider

# secfixes:
#   1.4.4-r0:
#     - CVE-2022-27650

build() {
	./configure \
		--prefix=/usr \
		--disable-systemd
	make
}

check() {
	make tests/tests_libcrun_errors.log
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
04777626873743d0f5d9d0c0338a37be03c786a4b57b95b83caba0a9ef5b64280194c4e7973fd6bb2a52a7aed458e2e76b4c638d1a9cf3e69ff7176d71037d23  crun-1.9.tar.xz
"

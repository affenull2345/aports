# Contributor: G.J.R. Timmer <gjr.timmer@gmail.com>
# Maintainer: G.J.R. Timmer <gjr.timmer@gmail.com>
pkgname=postgresql-pg_cron
pkgver=1.6.0
pkgrel=0
pkgdesc="Cron-based scheduler for PostgreSQL 9.5+"
url="https://github.com/citusdata/pg_cron"
arch="all"
license="PostgreSQL"
makedepends="postgresql-dev"
provides="pg_cron=$pkgver-r$pkgrel"
install="$pkgname.post-install"
source="https://github.com/citusdata/pg_cron/archive/v$pkgver/pg_cron-$pkgver.tar.gz"
builddir="$srcdir/pg_cron-$pkgver"
options="!check"  # no tests provided

prepare() {
	default_prepare
	# Remove -Werror from Makefile
	# Required to compile on alpine, to ignore compilation warnings
	sed "s/-Werror //" -i Makefile
}

build() {
	make
}

package() {
	depends="postgresql$(pg_config --major-version)"

	make DESTDIR="$pkgdir" install
	install -Dm644 LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}

sha512sums="
56e74d4fa9fadad9770d4935115530127f7d2eae8734e9fadf3048427d70988b1a8568821870dc33756c6ff34ee202e6897d77e5a7318be2c33052469c693113  pg_cron-1.6.0.tar.gz
"

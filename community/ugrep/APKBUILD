# Contributor: Francesco Camuffo <dev@fmac.xyz>
# Maintainer: Francesco Camuffo <dev@fmac.xyz>
pkgname=ugrep
pkgver=4.1.0
pkgrel=0
pkgdesc="Ultra fast grep with interactive query UI and fuzzy search"
url="https://github.com/Genivia/ugrep/wiki"
arch="all"
license="BSD-3-Clause"
checkdepends="bash"
makedepends="bzip2-dev lz4-dev pcre2-dev xz-dev zlib-dev zstd-dev"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/Genivia/ugrep/archive/refs/tags/v$pkgver.tar.gz"

build() {
	CXXFLAGS="$CXXFLAGS -O2 -flto=auto" \
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man
	make
}

check() {
	make test
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
036d6ff8b0ceb58c806cb8550ae8809fc799bc6476d8dba19ec5756826ea991cdd6326932dfa4267528776096b5ae7bdb0a7c2e3a3f9abd4b074a1cc4c5ba37b  ugrep-4.1.0.tar.gz
"

# Contributor: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
# Maintainer: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
pkgname=py3-opfunu
_pkgorig=opfunu
pkgver=1.0.0
pkgrel=4
pkgdesc="A collection of Benchmark functions for numerical optimization problems"
url="https://github.com/thieu1995/opfunu"
arch="noarch"
license="MIT"
depends="python3 py3-matplotlib py3-numpy py3-pandas py3-pillow py3-requests py3-scipy"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
checkdepends="py3-pytest-xdist"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/o/opfunu/opfunu-$pkgver.tar.gz
	new-numpy.patch
	"
builddir="$srcdir/$_pkgorig-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest -n auto
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl

	rm -r "$pkgdir"/usr/lib/python3.*/site-packages/examples
	rm -r "$pkgdir"/usr/lib/python3.*/site-packages/tests
}

sha512sums="
5df9e1e29391c4850f27f33c166ed4f4e32471ab094248cf8690a99492d044dc9b30c54e80086b6de6569e055573b0f2339e4097e6387a6a1888a490d6e6cce0  py3-opfunu-1.0.0.tar.gz
81221ba63fcb7419c41478657ffee559e674410a62fbef57dd97bdbcd02a499c85f7f849d831bfabc2aadad7eb2d3296606f61241efabb79a421445adf889617  new-numpy.patch
"

# Maintainer: team/gnome <david@ixit.cz>
# Contributor: David Heidelberg <david@ixit.cz>
pkgname=gnome-console
pkgver=44.4
pkgrel=0
pkgdesc="User-friendly terminal for GNOME"
url="https://gitlab.gnome.org/GNOME/console"
# s390x blocked by mozjs91 -> nautilus-dev
arch="all !s390x"
license="GPL-3.0-only"
depends="
	dbus
	gsettings-desktop-schemas
	"
makedepends="
	desktop-file-utils
	glib-dev
	gsettings-desktop-schemas-dev
	gtk4.0-dev
	libadwaita-dev
	libgtop-dev
	meson
	pcre2-dev
	sassc
	vte3-dev
	"
subpackages="$pkgname-lang"
source="https://download.gnome.org/sources/gnome-console/${pkgver%.*}/gnome-console-$pkgver.tar.xz"
replaces="kgx"
provides="kgx=$pkgver-r$pkgrel"

build() {
	abuild-meson \
		-Db_lto=true \
		-Dtests=true \
		. output
	meson compile -C output
}

check() {
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
3fbd7c16638e829103e2dc517df6d68c7acbaca24f7f2af4a4bfeb79ec79944a98432dbdc778ecefd74fded0a701be179c09af95d099dbad79484d907ab923c6  gnome-console-44.4.tar.xz
"

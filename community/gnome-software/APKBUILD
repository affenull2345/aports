# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: team/gnome <ablocorrea@hotmail.com>
pkgname=gnome-software
pkgver=44.4
pkgrel=0
pkgdesc="Software lets you install and update applications and system extensions"
url="https://wiki.gnome.org/Apps/Software"
arch="all"
license="GPL-2.0-or-later"
makedepends="
	appstream-dev
	flatpak-dev
	gdk-pixbuf-dev
	glib-dev
	gsettings-desktop-schemas-dev
	gtk4.0-dev
	gtk-doc
	json-glib-dev
	libadwaita-dev
	libgudev-dev
	libsoup3-dev
	libxmlb-dev
	meson
	ostree-dev
	polkit-dev
	"
options="!check" # lots of failing tests
install="$pkgname.post-upgrade"
subpackages="
	$pkgname-dbg
	$pkgname-lang
	$pkgname-doc
	$pkgname-dev
	$pkgname-lib
	$pkgname-plugin-flatpak:flatpak_plugin
	"
source="https://download.gnome.org/sources/gnome-software/${pkgver%.*}/gnome-software-$pkgver.tar.xz
	0001-disable-Automatic-Updates-options.patch
	0001-lib-remove-gs-profiler.h-from-the-public-API.patch
	org.gnome.software.gschema.override
	"

case "$CARCH" in
	x86|x86_64|aarch64|armv7) makedepends="$makedepends fwupd-dev" ;;
esac

build() {
	case "$CARCH" in
		x86|x86_64|aarch64|armv7) conf="-Dfwupd=true" ;;
		*) conf="-Dfwupd=false" ;;
	esac

	abuild-meson \
		-Dmalcontent=false \
		-Dpackagekit=false \
		-Dhardcoded_proprietary_webapps=false \
		-Dtests=false \
		$conf \
		. output
	meson compile -C output
}

check() {
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
	install -Dm644 "$srcdir"/org.gnome.software.gschema.override \
		-t "$pkgdir"/usr/share/glib-2.0/schemas/
}

dev() {
	default_dev

	amove usr/lib/gnome-software/libgnomesoftware.so
}

lib() {
	pkgdesc="$pkgdesc (shared library)"
	depends=""

	amove usr/lib/gnome-software/libgnomesoftware.so.*
}

flatpak_plugin() {
	pkgdesc="$pkgdesc (flatpak plugin)"
	install_if="$pkgname=$pkgver-r$pkgrel flatpak"
	depends=""

	amove usr/lib/gnome-software/plugins-20/libgs_plugin_flatpak.so
	amove usr/share/metainfo/org.gnome.Software.Plugin.Flatpak.metainfo.xml
}

sha512sums="
97eaa33b4dd91a6865ee7eeee28e5acf3cf5fbd6a72a60cf5a7c528f79f6f0684014d1169318314b8efce23c1c29e7d24a71193998ced930584a50494137cf8b  gnome-software-44.4.tar.xz
db843002263f208955f44f13c7de34f8fdded9ea43c87136611b4b082fef25bc5ac6d21bb9fcc55cf7d4cfc5b23ec2217b63a553c4059eea249bf20512891a76  0001-disable-Automatic-Updates-options.patch
e092e2a5bd514c3f54184b8d2d1811aebfa86772814cb403a1fd6342c27ea7a5f29921e6e86ce3fafde3a5d3e6ede89487276577aa6f07c9e5f32cab6e44548f  0001-lib-remove-gs-profiler.h-from-the-public-API.patch
392d13b84ffe0ce9502c3beac1d2dffd1eef6df88ca4b2c78b59152130db94480865f3a9743b7f24bb0e44f0c5a19f0d8a22c4363d1e1a55778870f9da744d28  org.gnome.software.gschema.override
"

# Contributor: omni <omni+alpine@hack.org>
# Maintainer: omni <omni+alpine@hack.org>
pkgname=arti
pkgver=1.1.8
pkgrel=0
pkgdesc="An implementation of Tor, in Rust"
url="https://docs.rs/arti/latest/arti/"
license="GPL-3.0-or-later"
# ppc64le & s390x blocked by ring crate
arch="all !ppc64le !s390x !riscv64"
makedepends="cargo openssl-dev>3 sqlite-dev zstd-dev xz-dev cargo-auditable"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://gitlab.torproject.org/tpo/core/arti/-/archive/arti-v$pkgver/arti-arti-v$pkgver.tar.gz"
builddir="$srcdir/$pkgname-$pkgname-v$pkgver"


prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked

	# Rust target triple.
	local target=$(rustc -vV | sed -n 's/host: //p')

	# Build against system-provided libs
	mkdir -p .cargo
	cat >> .cargo/config.toml <<-EOF
		[target.$target]
		zstd = { rustc-link-lib = ["zstd"] }
	EOF
}

build() {
	cargo auditable build --release --frozen --bin arti
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 -t "$pkgdir"/usr/bin/ target/release/arti
	install -Dm755 -t "$pkgdir"/usr/share/doc/"$pkgname"/ \
		doc/bridges.md doc/Compatibility.md doc/FAQ.md doc/SupportPolicy.md \
		CHANGELOG.md README.md
}

sha512sums="
4c978103657ba19ad451d7bc08d8e8cc4bbd0ade1d42992ac054072e23b10cde7e66944fb551bea24989eb6544fcf175359676a11f789d77306a7e05a383942e  arti-1.1.8.tar.gz
"

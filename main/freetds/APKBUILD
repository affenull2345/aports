# Contributor: Michael Mason <ms13sp@gmail.com>
# Maintainer: Michael Mason <ms13sp@gmail.com>
pkgname=freetds
pkgver=1.4
pkgrel=0
pkgdesc="Tabular Datastream Library"
url="https://www.freetds.org/"
arch="all"
license="GPL-2.0-or-later OR LGPL-2.0-or-later"
makedepends="openssl-dev>3 linux-headers readline-dev unixodbc-dev"
subpackages="$pkgname-static $pkgname-dev $pkgname-doc"
source="https://www.freetds.org/files/stable/freetds-$pkgver.tar.bz2"
options="!check"  # tests require running SQL server http://www.freetds.org/userguide/confirminstall.htm#TESTS

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--enable-msdblib \
		--with-openssl=/usr \
		--enable-odbc \
		--with-unixodbc=/usr
	make
}

check() {
	cd "$builddir"/src/replacements
	make check
}

package() {
	make -j1 DESTDIR="$pkgdir" install
}

sha512sums="
6d0e43c4289d33ef11302019d5b5e563fa1f8ae7aeab466d808ce6242c3859c44b80fc5143244e47fdf75d717f1ca7286a84b029f62df172273184c5d8a40065  freetds-1.4.tar.bz2
"
